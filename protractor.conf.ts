import {Config, browser} from "protractor";
/**
 * Created by Dmitrii_Khodakovskii on 11/23/2016.
 */

export let config: Config = {
    seleniumAddress: "http://localhost:4444/wd/hub",
    capabilities: {
        browserName: "chrome"
    },

    framework: "custom",
    frameworkPath: require.resolve("protractor-cucumber-framework"),
    specs: [
        "../src/test/features/**/*.feature"
    ],
    allScriptsTimeout: 500000,
    getPageTimeout: 60000,
    cucumberOpts: {
        require: "src/test/features/steps_definition/**/*.steps.js",
        format: "pretty"
    },

    onPrepare: function() {
        browser.ignoreSynchronization = true;
        browser.get('https://calendar.google.com/calendar');
        browser.manage().timeouts().pageLoadTimeout(60000);
    }

};