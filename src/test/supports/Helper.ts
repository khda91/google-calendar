import {LoginPage} from "../../pages/LoginPage";


export class Helper {

    private static loginPage: LoginPage = new LoginPage();

    public static login(username: string, password: string): void {
        this.loginPage.sendUserName(username);
        this.loginPage.nextButtonClick();
        this.loginPage.init();
        this.loginPage.sendPassword(password);
        this.loginPage.signInClick();
    }

    public static logout(): void {
        this.loginPage.clickByAccountLogo();
        this.loginPage.clickExitButton();
    }

}