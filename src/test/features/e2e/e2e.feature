#Feature: End-To-End
#
#  Scenario: Create new meeting request
#    Given Opened calendar page: "https://calendar.google.com/calendar"
#    When click create request button
#    And opened create request form: "https://calendar.google.com/calendar/"
#    And Enter meeting: meeting1
#    And start date: 11/24/2016, time: 2:00PM
#    And end date: 11/24/2016, time: 3:00PM
#    And place: place1
#    And description: desc
#    And status: Занят(а)
#    And security: Личное
#    And click button save
#    Then meeting meeting1 add to calendar
#
#  Scenario Outline: Create new meeting request
#    Given Opened calendar page: "https://calendar.google.com/calendar"
#    And click create request button
#    And opened create request form: "https://calendar.google.com/calendar/render#eventpage_6"
#    When Enter meeting: <name>
#    And start date: <startdate>, time: <starttime>
#    And end date: <enddate>, time: <endtime>
#    And place: <place>
#    And description: <desc>
#    And status: <status>
#    And security: <security>
#    And click button save
#    Then meeting <name> add to calendar

#    Examples:
#    |name|startdate|starttime|enddate|endtime|place|desc|status|security|
#    |meeting1|11/24/2016|2:00PM|11/24/2016|3:00PM|place1|desc1|Занят(а)|Личное|
#    |meeting2|11/26/2016|9:00AM|11/26/2016|4:00PM|place2|desc2|Занят(а)|Личное|
#    |meeting3|11/27/2016|8:00AM|11/30/2016|1:00PM|place3|desc3|Занят(а)|Личное|
#
#
#  Scenario: Create meeting request from calendar
#    Given Opened calendar page
#    And Click on calendar
#    And Show meeting info window
#    When I input meeting: "Ужин"
#    And I click button "Создать"
#    Then meeting add to calendar
#
#
#  Scenario: Delete meeting request from calendar
#    Given Opened calendar page
#    And Click on meeting
#    And Show meeting info window
#    When I click button "Удалить"
#    Then meeting delete from calendar
#
#
#  Scenario: Edit meeting request from calendar
#    Given Opened calendar page
#    And Click on meeting
#    And Show meeting info window
#    When I click button "Изменить мероприятие"
#    And Opened meeting edit page
#    And change meeting caption to "Новая встреча"
#    And change place to "wfh"
#    And click button "Сохранить"
#    Then meeting updated in calendar
#
#
#  Scenario: Check meeting for next week from calendar
#    Given Opened calendar page
#    And Click next week button
#    And displayed next week
#    And click oth meeting
#    Then meeting exist in calendar
