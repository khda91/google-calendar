import {given, binding, before, then, after} from "cucumber-tsflow";
import {LoginPage} from "../../../pages/LoginPage";
import {CalendarPage} from "../../../pages/CalendarPage";

let chai = require('chai').use(require('chai-as-promised'));
let expect = chai.expect;

// @binding()
class TestClass {

    private loginPage: LoginPage = new LoginPage();
    private calendar: CalendarPage = new CalendarPage();

    // @before()
    public login(): void {
        this.loginPage.sendUserName('testTester98210');
        this.loginPage.nextButtonClick();
        this.loginPage.init();
        this.loginPage.sendPassword('QWEqwe123');
        this.loginPage.signInClick();
    }

    // @given(/^Google calendar page open "([^"]*)"$/)
    public googleCalendarOpened(url: string) {
        return this.calendar.getCurrentURL().then((currentURL) =>
            expect(currentURL).to.contains(url));
    }

    // @then(/^Text in logo of page equals "([^"]*)"$/)
    public assertLogoText(logoText: string) {
        // return this.calendar.getMainLogoCaption().getText().then((data) =>
        //     expect(data.toLowerCase()).to.equal(logoText.toLowerCase()));
    }

    // @after()
    public logout(): void {
        this.loginPage.clickByAccountLogo();
        this.loginPage.clickExitButton();
    }
};

// export = TestClass;