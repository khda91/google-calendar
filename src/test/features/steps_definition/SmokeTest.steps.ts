import {before, binding, after, given, when} from "cucumber-tsflow";
import {Helper} from "../../supports/Helper";
import {CalendarPage} from "../../../pages/CalendarPage";
import {UserCredentials} from "../../supports/UserCredentials";
let chai = require('chai').use(require('chai-as-promised'));
let expect = chai.expect;

@binding()
class SmokeTest {

    private calendar: CalendarPage;

    @before()
    public setUp(): void {
        Helper.login(UserCredentials.TEST_USER_NAME, UserCredentials.TEST_USER_PASSWORD);
        this.calendar = new CalendarPage();
    }

    @given(/^Open Google Calendar page$/)
    public openCalendarPage(): void {
        return this.calendar.getCurrentURL().then(function (currentUrl) {
            console.log(currentUrl);
        });
    }

    @when(/^Click button "Создать"$/)
    public testMethod(): void {

        // this.calendar.mainNav.topRightNavigationElements.selectCalendarMode(CalendarMode.DAY);
        // this.calendar.mainNav.topLeftNavigationElements.clickNextPeriodButton();

        this.calendar.leftBar.currentMonth.getText().then(function (txt) {
            console.log(txt);
        });

        this.calendar.leftBar.nextMonth();
        this.calendar.leftBar.currentMonth.getText().then(function (txt) {
            console.log(txt);
        });

        // this.calendar.getNavigationElement().nextMonth();
        // this.calendar.getNavigationElement().getCurrentMonth().getText().then(function (txt) {
        //     console.log(txt);
        // });
        //
        // this.calendar.getNavigationElement().previousMonth();
        // this.calendar.getNavigationElement().getCurrentMonth().getText().then(function (txt) {
        //     console.log(txt);
        // });
    }

    @after()
    public setOff(): void {
        Helper.logout();
    }

};

export = SmokeTest;