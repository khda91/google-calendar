import {element, by} from "protractor";
import {NavigationElementsProperties} from "./properties/NavigationElementsProperties";


export class NavigationElement {

    private root;
    private createRequestButton;
    private calendarView;
    private nextMonthButton;
    private previousMonthButton;
    private _currentMonth;

    constructor() {
        this.root = element(NavigationElementsProperties.NAVIGATION);
        this.createRequestButton = this.root.element(NavigationElementsProperties.REQUEST_BUTTON);
        this.calendarView = this.root.element(NavigationElementsProperties.CALENDAR_VIEW);
        this.nextMonthButton = this.calendarView.element(NavigationElementsProperties.NEXT_MONTH_BUTTON);
        this.previousMonthButton = this.calendarView.element(NavigationElementsProperties.PREVIOUS_MONTH_BUTTON);
        this._currentMonth = this.calendarView.element(NavigationElementsProperties.CURRENT_MONTH);
    }

    public createMeetingRequest(): void {
        this.createRequestButton.click();
    }

    public nextMonth(): void {
        this.nextMonthButton.click();
    }

    public previousMonth(): void {
        this.previousMonthButton.click();
    }

    public selectDay(day: string): void {
        let selectedDayElement = this.calendarView.element(by.xpath('/td[contains(@class, \'dp-onmonth\') and text()=\'' + day + '\']'));
        selectedDayElement.click();
    }

    get currentMonth() {
        return this._currentMonth;
    }
}