import {element, by} from "protractor";
import {AccountElementsProperties} from "./properties/AccountElementsProperties";

export class AccountElement {

    private account;
    private accountInfo;
    private exitButton;

    constructor () {
        this.account = element(AccountElementsProperties.ACCOUNT);
        this.accountInfo = element(AccountElementsProperties.ACCOUNT_INFO);
        this.exitButton = this.accountInfo.element(AccountElementsProperties.EXIT);
    }

    public accountLogoClick(): void {
        this.account.click();
    }

    public clickExitButton(): void {
        this.exitButton.click();
    }

}
