export enum CalendarMode {
    DAY = <any>'День',
    WEEK = <any>'Неделя',
    MONTH = <any>'Месяц',
    FOUR_DAY = <any>'4 дня',
    TIMETABLE = <any>'Расписание'

}