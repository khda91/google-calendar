import {element} from "protractor";
import {ToolbarElementsProperties} from "./properties/ToolbarElementsProperties";
import {TopLeftToolbarNavigationElements} from "./TopLeftToolbarNavigationElements";
import {TopRightToolbarNavigationElements} from "./TopRightToolbarNavigationElements";

export class ToolbarElement {

    private root;
    private _logo;
    private _topLeftNavigationElements: TopLeftToolbarNavigationElements;
    private _topRightNavigationElements: TopRightToolbarNavigationElements;

    constructor () {
        this.root = element(ToolbarElementsProperties.MAIN_NAVIGATION);
        this._logo = this.root.element(ToolbarElementsProperties.LOGO);
        this._topLeftNavigationElements = new TopLeftToolbarNavigationElements(ToolbarElementsProperties.TOP_LEFT_TOOLBAR_NAVIGATION);
        this._topRightNavigationElements = new TopRightToolbarNavigationElements(ToolbarElementsProperties.TOP_RIGHT_TOOLBAR_NAVIGATION);
    }

    get logo() {
        return this._logo;
    }

    get topLeftNavigationElements(): TopLeftToolbarNavigationElements {
        return this._topLeftNavigationElements;
    }

    get topRightNavigationElements(): TopRightToolbarNavigationElements {
        return this._topRightNavigationElements;
    }
}