import {element} from "protractor";
import {TopRightToolbarNavigationElementsProperties} from "./properties/TopRightToolbarNavigationElementsProperties";
import {CalendarMode} from "./enums/CalendarMode";

export class TopRightToolbarNavigationElements {

    private root;
    private dayButton;
    private weekButton;
    private monthButton;
    private fourDaysButton;
    private timetableButton;

    constructor(locator) {
        this.root = element(locator);
        this.dayButton = element(TopRightToolbarNavigationElementsProperties.DAY_BUTTON);
        this.weekButton = element(TopRightToolbarNavigationElementsProperties.WEEK_BUTTON);
        this.monthButton = element(TopRightToolbarNavigationElementsProperties.MONTH_BUTTON);
        this.fourDaysButton = element(TopRightToolbarNavigationElementsProperties.FOUR_DAYS_BUTTON);
        this.timetableButton = element(TopRightToolbarNavigationElementsProperties.TIMETABLE_BUTTON);
    }

    public selectCalendarMode(mode: CalendarMode): void {
        switch (mode) {
            case CalendarMode.DAY:
                this.switchToDayMode();
                break;

            case CalendarMode.WEEK:
                this.switchToWeekMode();
                break;

            case CalendarMode.MONTH:
                this.switchToMonthMode();
                break;

            case CalendarMode.FOUR_DAY:
                this.switchToFourDaysMode();
                break;

            case CalendarMode.TIMETABLE:
                this.switchToTimetableMode();
                break;
        }
    }

    private switchToDayMode(): void {
        this.dayButton.click();
    }

    private switchToWeekMode(): void {
        this.weekButton.click();
    }

    private switchToMonthMode(): void {
        this.monthButton.click();
    }

    private switchToFourDaysMode(): void {
        this.fourDaysButton.click();
    }

    private switchToTimetableMode(): void {
        this.timetableButton.click();
    }

}