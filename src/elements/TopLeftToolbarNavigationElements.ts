import {element} from "protractor";
import {TopLeftToolbarNavigationElementsProperties} from "./properties/TopLeftToolbarNavigationElementsProperties";
export class TopLeftToolbarNavigationElements {

    private root;
    private todayButton;
    private previousPeriodButton;
    private nextPeriodButton;
    private _currentPeriod;

    constructor(locator) {
        this.root = element(locator);
        this.todayButton = this.root.element(TopLeftToolbarNavigationElementsProperties.TODAY_BUTTON);
        this.previousPeriodButton = this.root.element(TopLeftToolbarNavigationElementsProperties.PREVIOUS_PERIOD);
        this.nextPeriodButton = this.root.element(TopLeftToolbarNavigationElementsProperties.NEXT_PERIOD);
        this._currentPeriod = this.root.element(TopLeftToolbarNavigationElementsProperties.CURRENT_PERIOD);
    }

    get currentPeriod() {
        return this._currentPeriod;
    }

    clickTodayButton(): void {
        this.todayButton.click();
    }

    clickPreviousPeriodButton(): void {
        this.previousPeriodButton.click();
    }

    clickNextPeriodButton(): void {
        this.nextPeriodButton.click();
    }

}