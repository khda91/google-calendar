import {by} from "protractor";

export class ToolbarElementsProperties {

    public static MAIN_NAVIGATION = by.id('vr-nav');

    public static LOGO = by.className('applogo');

    public static TOP_LEFT_TOOLBAR_NAVIGATION = by.id('topLeftNavigation');

    public static TOP_RIGHT_TOOLBAR_NAVIGATION = by.id('topRightNavigation');
}