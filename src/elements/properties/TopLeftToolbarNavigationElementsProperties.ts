import {by} from "protractor";

export class TopLeftToolbarNavigationElementsProperties {

    static TODAY_BUTTON = by.className('date-nav-today');

    static PREVIOUS_PERIOD = by.className('date-nav-prev');

    static NEXT_PERIOD = by.className('date-nav-next');

    static CURRENT_PERIOD = by.xpath('//td[contains(@id, \'dateBox\')]');

}