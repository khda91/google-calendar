import {by} from "protractor";
export class NavigationElementsProperties {

    static NAVIGATION = by.id('nav');

    static REQUEST_BUTTON = by.id('createEventButtonContainer');

    static CALENDAR_VIEW = by.className('dp-monthtablediv monthtableSpace');

    static NEXT_MONTH_BUTTON = by.xpath('//span[contains(@class, \'dp-sb-next\')]');

    static PREVIOUS_MONTH_BUTTON = by.xpath('//span[contains(@class, \'dp-sb-prev\')]');

    static CURRENT_MONTH = by.className('calHeaderSpace');

}