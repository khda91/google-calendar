import {by} from "protractor";
import {CalendarMode} from "../enums/CalendarMode";

export class TopRightToolbarNavigationElementsProperties {

    static DAY_BUTTON = by.xpath('//div[text()=\'' + CalendarMode.DAY +'\']');

    static WEEK_BUTTON = by.xpath('//div[text()=\'' + CalendarMode.WEEK +'\']');

    static MONTH_BUTTON = by.xpath('//div[text()=\'' + CalendarMode.MONTH +'\']');

    static FOUR_DAYS_BUTTON = by.xpath('//div[text()=\'' + CalendarMode.FOUR_DAY +'\']');

    static TIMETABLE_BUTTON = by.xpath('//div[text()=\'' + CalendarMode.TIMETABLE +'\']');

}