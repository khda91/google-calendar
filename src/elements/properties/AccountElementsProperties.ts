import {by} from "protractor";
export class AccountElementsProperties {

    static ACCOUNT = by.xpath('//a[contains(@title, \'Аккаунт Google:\')]');

    static ACCOUNT_INFO = by.xpath('//div[contains(@aria-label, \'Информация об аккаунте\')]');

    static EXIT = by.id('gb_71');

}