import {ElementFinder, by} from "protractor";
export class TaskElement {

    private taskTime;
    private taskName;
    private taskPlace;
    private reminder;
    private privacy;

    constructor(rootElement: ElementFinder) {
        this.taskName = rootElement.$$('dd').getText();
    }

    public getText(): String {
        return this.taskName;
    }

}