import {element, by, ElementFinder} from "protractor";
import {TaskElement} from "./TaskElement";

export class CalendarElement {

    private calendar;
    protected tasks: Array<TaskElement>;

    constructor() {
        this.calendar = element(by.id('gridcontainer'));
        this.tasks = new Array<TaskElement>();
    }

    public initTasks(): void {

    }

    public addTasksToArray(element: ElementFinder): void {
        this.tasks.push(new TaskElement(element));
    }
}