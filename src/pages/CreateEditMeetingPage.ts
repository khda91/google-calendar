import {element, by, browser} from "protractor";


export class CreateEditMeetingPage {

    private saveBtn;
    private requestCaptionField;
    private startDateField;
    private startTimeField;
    private endTimeField;
    private endDateField;
    private placeField;
    private descriptionField;
    private statusField;
    private privacyField;

    constructor(statusText?: string, privacyText?: string) {
        this.init(statusText, privacyText);
    }

    public reInit() {
        this.init();
    }

    private init(statusText?: string, privacyText?: string): void {
        let status = 'Занят(а)';
        let privacy = 'Личное';
        if (statusText != '')
            status = statusText;
        if (privacyText != '')
            privacy = privacyText;

        this.saveBtn = element(by.id(':z0.save_top'));
        this.requestCaptionField = element(by.xpath('//input[@class=\'textinput ui-placeholder\' and @title=\'Название мероприятия\']'));
        this.startDateField = element(by.xpath('//input[@class=\'text dr-date\' and @title=\'Дата начала\']'));
        this.startTimeField = element(by.xpath('//input[@class=\'text dr-time\' and @title=\'Время начала\']'));
        this.endTimeField = element(by.xpath('//input[@class=\'text dr-time\' and @title=\'Время окончания\']'));
        this.endDateField = element(by.xpath('//input[@class=\'text dr-date\' and @title=\'Дата окончания\']'));
        this.placeField = element(by.id(':zk'));
        this.descriptionField = element(by.id(':zl'));
        this.statusField = element(by.xpath('//label[@class=\'ep-dp-radio\' and text()=\'' + status + '\']'));
        this.privacyField = element(by.xpath('//label[@class=\'ep-dp-radio\' and text()=\'' + privacy + '\']'));
    }

    public getCurrentURL(): any {
        return browser.getCurrentUrl();
    }

    public save(): void {
        this.saveBtn.click();
    }

    public enterRequestCaption(value: string): void {
        this.requestCaptionField.sendKeys(value);
    }

    public enterStartDate(value: string): void {
        this.startDateField.sendKeys(value);
    }

    public enterStartTime(value: string): void {
        this.startTimeField.sendKeys(value);
    }

    public enterEndDate(value: string): void {
        this.endDateField.sendKeys(value);
    }

    public enterEndTime(value: string): void {
        this.endTimeField.sendKeys(value);
    }

    public enterPlace(value: string): void {
        this.placeField.sendKeys(value);
    }

    public enterDescription(value: string): void {
        this.descriptionField.sendKeys(value);
    }

    public selectStatus(): void {
        this.statusField.click();
    }

    public selectPrivacy():void {
        this.privacyField.click();
    }

}
