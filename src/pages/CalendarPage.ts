import {browser} from "protractor";
import {ToolbarElement} from "../elements/ToolbarElement";
import {CalendarElement} from "../elements/CalendarElement";
import {NavigationElement} from "../elements/NavigationElement";

export class CalendarPage {

    private _mainNav: ToolbarElement;
    private calendar: CalendarElement;
    private _leftBar: NavigationElement;

    constructor() {
        this._mainNav = new ToolbarElement();
        this.calendar = new CalendarElement();
        this._leftBar = new NavigationElement();
    }

    get leftBar(): NavigationElement {
        return this._leftBar;
    }

    public getCurrentURL(): any {
        return browser.getCurrentUrl();
    }

    get mainNav(): ToolbarElement {
        return this._mainNav;
    }

}