import {element, by, browser, wrapDriver} from "protractor";
import {AccountElement} from "../elements/AccountElement";
import {LoginPageProperties} from "./properties/LoginPageProperties";


export class LoginPage {

    private account: AccountElement;
    private userNameField;
    private passwordField;
    private nextButton;
    private signInButton;

    constructor() {
        this.userNameField = element(LoginPageProperties.EMAIL_FIELD);
        this.nextButton = element(LoginPageProperties.NEXT_BUTTON);
        this.passwordField = element(LoginPageProperties.PASSWORD_FIELD);
        this.signInButton = element(LoginPageProperties.SING_IN_BUTTON);
        this.account = new AccountElement();
    }

    public sendUserName(username: string): void {
        this.userNameField.sendKeys(username);
    }

    public nextButtonClick(): void {
        this.nextButton.click();
    }

    public init(): void {
        browser.driver.sleep(500);
    }

    public sendPassword(passwd: string): void {
        this.passwordField.sendKeys(passwd);
    }

    public signInClick(): void {
        this.signInButton.click();
    }

    public clickByAccountLogo(): void {
        this.account.accountLogoClick();
    }

    public clickExitButton(): void {
        this.account.clickExitButton();
    }

}
