import {by} from "protractor";

export class LoginPageProperties {

    static EMAIL_FIELD = by.id("Email");

    static PASSWORD_FIELD = by.id('Passwd');

    static NEXT_BUTTON = by.id("next");

    static SING_IN_BUTTON = by.id('signIn');

}